//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;

uniform sampler2D tex;
uniform vec4 color;
uniform vec4 redColor;

uniform bool redFl;


void main() {

	if (redFl) {
		vec4 col = texture2D(tex, texCoords);
		if (col.r == 1) {
			FragColor = col*color;
		}
		else {
			FragColor = col*redColor;
		}
	}
	else {
		FragColor = texture2D(tex, texCoords);
	}

}