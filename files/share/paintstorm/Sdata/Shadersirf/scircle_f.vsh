//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;


uniform vec4 color;

uniform float rad;
uniform float sz;
uniform float frameWidth;


void main() {

	FragColor = color;
	
	vec2 cor = vec2(texCoords.x*sz, texCoords.y*sz);

	float dist = distance(cor, vec2(sz / 2));

	if (frameWidth == 0) {

		if (dist > rad) {
			FragColor.a = 0;
			return;
		}

		if (rad - dist < 1) {
			FragColor.a *= (rad - dist) / 1;
		}
	}
	else {//---------------------------
		float rad2 = rad + frameWidth;

		if (dist < rad || dist>rad2) {
			FragColor.a = 0;
			return;
		}

		if (dist-rad < 1) {
			FragColor.a *= (dist-rad);
		}

		if (rad2 - dist < 1) {
			FragColor.a *= rad2 - dist;
		}

	}
		
}