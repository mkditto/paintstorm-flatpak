//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;


uniform sampler2D tex;

void main() {

	FragColor = texture2D(tex, texCoords);

}