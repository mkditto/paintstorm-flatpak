//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;


uniform sampler2D blendTexture;
uniform sampler2D blurTexture;


uniform float size;
uniform float putw;
uniform float texsize;

uniform float brush_hale_cur;
uniform float brush_coloramount_cur;
uniform bool brush_pickunder;
uniform vec4 brush_curcolor;
uniform vec4 maincolor;

uniform float oldputw;

uniform bool blur0fl;


////////////////////////////////////////////
////////////////////////////////////
vec4 bilinear(sampler2D tex) {

	vec2 cor = texCoords*oldputw;
	

	float dx = cor.x - int(cor.x);
	float dy = cor.y - int(cor.y);

	cor.x = int(cor.x) + 0.5;
	cor.y = int(cor.y) + 0.5;

	vec4 col0, col1, col2, col3;
	col0 = col1 = col2 = col3 = vec4(0);

	if (dx <= 0.5 && dy <= 0.5) {
		col0 = texture2D(tex, vec2(cor.x - 1, cor.y - 1) /texsize);
		col1 = texture2D(tex, vec2(cor.x, cor.y - 1) /texsize);
		col2 = texture2D(tex, vec2(cor.x, cor.y) /texsize);
		col3 = texture2D(tex, vec2(cor.x - 1, cor.y) /texsize);
		dx += 0.5;
		dy += 0.5;
	}
	else
		if (dx >= 0.5 && dy <= 0.5) {
			col0 = texture2D(tex, vec2(cor.x, cor.y - 1) /texsize);
			col1 = texture2D(tex, vec2(cor.x + 1, cor.y - 1) /texsize);
			col2 = texture2D(tex, vec2(cor.x + 1, cor.y) /texsize);
			col3 = texture2D(tex, vec2(cor.x, cor.y) /texsize);
			dx -= 0.5;
			dy += 0.5;
		}
		else
			if (dx <= 0.5 && dy >= 0.5) {
				col0 = texture2D(tex, vec2(cor.x - 1, cor.y) /texsize);
				col1 = texture2D(tex, vec2(cor.x, cor.y) /texsize);
				col2 = texture2D(tex, vec2(cor.x, cor.y + 1) /texsize);
				col3 = texture2D(tex, vec2(cor.x - 1, cor.y + 1) /texsize);
				dx += 0.5;
				dy -= 0.5;
			}
			else
				if (dx >= 0.5 && dy >= 0.5) {
					col0 = texture2D(tex, vec2(cor.x, cor.y) /texsize);
					col1 = texture2D(tex, vec2(cor.x + 1, cor.y) /texsize);
					col2 = texture2D(tex, vec2(cor.x + 1, cor.y + 1) /texsize);
					col3 = texture2D(tex, vec2(cor.x, cor.y + 1) /texsize);
					dx -= 0.5;
					dy -= 0.5;
				}




	if (col0.a == 0 || col1.a == 0 || col2.a == 0 || col3.a == 0) {

		float sum = col0.a + col1.a + col2.a + col3.a;
		if (sum != 0) {

			vec4 midcol = (col0*col0.a + col1*col1.a + col2*col2.a + col3*col3.a) / sum;

			col0.r = col0.r*col0.a + midcol.r*(1 - col0.a);
			col0.g = col0.g*col0.a + midcol.g*(1 - col0.a);
			col0.b = col0.b*col0.a + midcol.b*(1 - col0.a);

			col1.r = col1.r*col1.a + midcol.r*(1 - col1.a);
			col1.g = col1.g*col1.a + midcol.g*(1 - col1.a);
			col1.b = col1.b*col1.a + midcol.b*(1 - col1.a);

			col2.r = col2.r*col2.a + midcol.r*(1 - col2.a);
			col2.g = col2.g*col2.a + midcol.g*(1 - col2.a);
			col2.b = col2.b*col2.a + midcol.b*(1 - col2.a);

			col3.r = col3.r*col3.a + midcol.r*(1 - col3.a);
			col3.g = col3.g*col3.a + midcol.g*(1 - col3.a);
			col3.b = col3.b*col3.a + midcol.b*(1 - col3.a);
		}
		else {
			return vec4(0, 0, 0, 0);
		}

	}

	float p0 = (1 - dx)*(1 - dy);
	float p1 = (dx)*(1 - dy);
	float p2 = (dx)*(dy);
	float p3 = (1 - dx)*(dy);

	return   (col0*p0 + col1*p1 + col2*p2 + col3*p3);

}
////////////////////////////////////
////////////////////////////////////////
vec4 sum3col(vec4 col0, vec4 col1, vec4 col2, float dx) {
	
	float sum = col0.a + col1.a + col2.a;

	if (sum < 3 && sum!=0) {
		vec4 midcol = (col0*col0.a + col1*col1.a + col2*col2.a) / sum;

		float a0 = col0.a;
		float a1 = col1.a;
		float a2 = col2.a;
		col0 = col0*col0.a + midcol*(1 - col0.a);
		col1 = col1*col1.a + midcol*(1 - col1.a);
		col2 = col2*col2.a + midcol*(1 - col2.a);
		col0.a = a0;
		col1.a = a1;
		col2.a = a2;
	}


	return (col0*(1 - dx) + col1 + col2*dx) / 2;

}
////////////////////////////////////////
void main(){

	vec2 cor = texCoords*size/texsize;

	float dx = texCoords.x*size;
	dx = dx-int(dx);

	float dy = texCoords.y*size;
	dy = dy - int(dy);

	float d =  1 / texsize;

	float corx_md = cor.x - d;
	float cory_md = cor.y - d;
	if (corx_md < 0) { corx_md = 0; }
	if (cory_md < 0) { cory_md = 0; }

	float corx_pd = cor.x + d;
	float cory_pd = cor.y + d;
	if (corx_pd > size/texsize) { corx_pd = (size-0.001) / texsize; }
	if (cory_pd > size/texsize) { cory_pd = (size-0.001) / texsize; }


	vec4 p00 = texture2D(blendTexture, vec2(corx_md , cory_md) );
	vec4 p01 = texture2D(blendTexture, vec2(cor.x, cory_md) );
	vec4 p02 = texture2D(blendTexture, vec2(corx_pd, cory_md));

	vec4 col0 = sum3col(p00, p01, p02, dx);

	vec4 p10 = texture2D(blendTexture, vec2(corx_md, cor.y));
	vec4 p11 = texture2D(blendTexture, vec2(cor.x, cor.y));
	vec4 p12 = texture2D(blendTexture, vec2(corx_pd, cor.y));

	vec4 col1 = sum3col(p10, p11, p12, dx);

	vec4 p20 = texture2D(blendTexture, vec2(corx_md, cory_pd));
	vec4 p21 = texture2D(blendTexture, vec2(cor.x, cory_pd));
	vec4 p22 = texture2D(blendTexture, vec2(corx_pd, cory_pd));

	vec4 col2 = sum3col(p20, p21, p22, dx);




	if (brush_hale_cur < 0.99) {

		vec4 color = vec4(0);
		if (blur0fl) {
			color = texture2D(blendTexture, cor);
		}
		else {
			color = sum3col(col0, col1, col2, dy);
		}

		vec4 oldcol = bilinear(blurTexture);
		

		vec4 sumcol = (color*color.a + oldcol*oldcol.a) / (color.a+oldcol.a);

		
		color = color*color.a + sumcol*(1 - color.a);
		oldcol = oldcol*oldcol.a + sumcol*(1 - oldcol.a);
		
		float t = brush_hale_cur;
		
		
			FragColor = color*t + oldcol*(1 - t);
		
	}
	else {
		FragColor = sum3col(col0, col1, col2, dy);
	}
	
	vec4 brc = brush_curcolor;

	if (!brush_pickunder && FragColor.a<1 && brush_coloramount_cur>0.001) {
		
		vec4 sumcol = (FragColor*FragColor.a + brc*brc.a) / (FragColor.a + brc.a);
		FragColor.r = FragColor.r*FragColor.a + sumcol.r*(1 - FragColor.a);
		FragColor.g = FragColor.g*FragColor.a + sumcol.g*(1 - FragColor.a);
		FragColor.b = FragColor.b*FragColor.a + sumcol.b*(1 - FragColor.a);
		brc.r = brc.r*brc.a + sumcol.r*(1 - brc.a);
		brc.g = brc.g*brc.a + sumcol.g*(1 - brc.a);
		brc.b = brc.b*brc.a + sumcol.b*(1 - brc.a);

	}
	
		

	//-----F-------------
	if (maincolor.r != -1) {
		FragColor = maincolor;
	}
	//--------------

	
	
	
	
}
