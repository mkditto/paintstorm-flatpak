//Copyright (C) 2014-2017 Paintstorm Studio
#version 150

in vec3 coord;
out vec2 texCoords; 
void main() {
	gl_Position = vec4(coord, 1.0);
	gl_Position.xy += coord.xy;
	texCoords = coord.xy + vec2(0.5, 0.5);
}