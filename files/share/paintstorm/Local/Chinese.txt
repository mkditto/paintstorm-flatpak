﻿#000#画笔#Brush 
#001#基本#GENERAL
#002#间距，分散#SPACING JITTER
#003#角#ANGLE
#004#画笔外形#BRUSH FORM
#005#颜色#COLOR
#006#纹理#TEXTURE
#007#尺寸#Size
#008#对比质感#Tex. Contrast
#009#浑浊度#Opacity
#010#漆数#Color amount漆数
#011#可塑性#Extendtion
#012#稳定剂#Stabilizer
#013#透明度#Transparency
#014#间距#Spacing
#015#分散#Scatter
#016#分散密度#Scat. Density
#017#角 1#Angle 1
#018#角 2#Angle 2
#019#X压缩#SqueezeX
#020#Y压缩#SqueezeY
#021#关系尺寸#Connect sizes
#022#双轴#Both axis
#023#固定#Fixation
#024#使用遮罩刷#Use mask brush
#025#反转#Invert
#026#考虑下层#Pick under
#027#混合透明度#Blend transp.
#028#脏画笔#Dirty mode
#029#关系尺寸#Size connection
#030#保护主色#Preserve main color
#031#随机尺寸#Rand scale
#032#随机移动#Rand offset
#033#反转#Invert
#034#圆形#Circle
#035#任意形式#Custom form
#036#橡皮擦#Eraser
#037#普通#Normal (blend mode) 
#038#倍增#Multiply
#039#绒毛数#Feature (Villuses count) 
#040#模型组#Set of forms
#041#梯度#Gradient
#042#中风梯度#Use stroke gradient
#043#圆形#Use radial
#044#线性#Use linear
#045#绒毛形状#Villus diff. (relief villi) 
#046#比例#Scale
#047#污染强度#Dirty strength
#048#纹理#Use texture
#049#纹理效果#Tex. Strength
#050#遮罩效果#Mask strength
#051# 普通# Normal
#052# 衰减# Dissolve
#053# 遮光# Darken
#054# 倍增# Multiply
#055# 底图遮光# Color Burn
#056# 线性调光器# Linear Burn
#057# 更深# Darker Color
#058# 浅色替代# Lighten
#059# 屏幕# Screen
#060# 底图光泽处理# Color Dodge
#061# 添加# Add
#062# 更浅# Lighter Color
#063# 覆盖# Overlay
#064# 柔和光线# Soft Light
#065# t强光# Hard Ligh
#066# 明亮光线# Vivid Light
#067# 线性光线# Linear Light
#068# 射灯# Pin Light
#069# 强烈混合# Hard Mix
#070# 差别# Difference
#071# 例外# Exclusion
#072# 减去# Substract
#073# 分散# Divide
#074# 色调# Hue
#075# 饱和度# Saturation
#076# 色度# Color
#077# 亮度# Brightness
#078#画笔#Brushes
#079#辅助#Reference
#080#宽度：#Width:
#081#高度：#Height:
#082#取消#   Cancel
#083#颜色#Color
#084#蔚蓝—红#Cyan-red
#085#紫—绿#Purple-green
#086#黄—蓝#Yellow-blue
#087#亮度#Brightness
#088#对比度#Contrast
#089#阴影#Shadows
#090#中间调#Midtones
#091#亮色调#Highlights
#092#色调#Hue
#093#饱和度#Saturation
#094#亮度#Brihgtness
#095#非饱和饱和度#Saturation of unsuturated
#096#中饱和饱和度#Saturation of medium suturated
#097#饱和饱和度#Saturation of full suturated
#098#客户 1#Custom 1
#099#客户 2#Заказная 2
#100#客户 3#Заказная 3
#101#工具#Tools
#102#画笔#Brush
#103#底布#Canvas
#104#图层及选中#Layers and selection
#105#工作区#Workspace
#106#其它#Other
#107# -空-# -None- (Empty)
#108#未命名#Untitled
#109#名称：#Name:
#110#宽度：#Width:
#111#高度：#Height:
#112#梯度#Gradients
#113#浑浊度#Opacity
#114#图层#Layers
#115#普通#Normal
#116#浑浊度#Opacity
#117#图像#Graph
#118#宽度：#Width:
#119#高度：#Height:
#120#范围#Range
#121#画笔压力#Pen pressure
#122#画笔方向#Pen direction
#123#画笔倾斜度#Pen tilt
#124#画笔转动#Pen rotation
#125#涂画方向#Stroke direction
#126#顺向#Link to guides
#127#涂画速度#Stroke speed
#128#随机#Random
#129#过渡#Transition
#130#尺寸#Scale
#131#后期校正#Post correction
#132#前一步#Undo
#133#后一步#Redo
#134#复制#Copy
#135#粘贴#Paste
#136#剪切#Cut
#137#新建#New
#138#打开#Open
#139#保存#Save
#140#另存为#Save As
#141#快键#Define Hotkeys
#142#设置#Options
#143#退出#Exit
#144#色平衡，亮度#Color balance, brightness
#145#色调和饱和度#Hue and Saturation
#146#底布尺寸#Canvas resize
#147#图像尺寸#Image resize
#148#自由变换#Free transform
#149#新图层#New Layer
#150#新文件夹#New Folder
#151#新遮罩层#New Mask Layer
#152#删除图层#Delete Laye
#153#复制图层#Duplicate layer
#154#下层合并#Merge Layer Down
#155#帮助#Help
#156#关于软件#About
#157#开启向导#Enable guides
#158#跟随向导#Snap to guides
#159#检查更新#Check for updates
#160#清除#Clear
#161#反转#Invert
#162#选中全部#Select All
#163#隐藏选中#Hide Selection
#164#使用辅助图层#Use Reference Layer
#165#自动隐藏选中#Auto hide selection
#166#重置#Reset View
#167#翻转#Flip View
#168#控制板窗口#Panels enabler/disabler
#169#工具#Tools
#170#工具参数#Tool options
#171#画笔参数#Brush options
#172#图层#Layer
#173#色轮#Color wheel
#174#导航#Navigator
#175#图像样板#Graphs presets
#176#调色盘#Mixer
#177#画笔#Brushes
#178#颜色样板#Color swatches
#179#辅助#Reference
#180#客户 1#Заказная 1
#181#客户 2#Заказная 2
#182#客户 3#Заказная 3
#183#工作区 1#Рабочее пространство 1
#184#工作区 2#Рабочее пространство 2
#185#工作区 3#Рабочее пространство 3
#186#工作区 4#Рабочее пространство 4
#187#工作区 5#Рабочее пространство 5
#188#工作区 6#Рабочее пространство 6
#189#快键 1#Keyboard preset 1
#190#快键 2#Keyboard preset 2
#191#快键 3#Keyboard preset 3
#192#隐藏窗口#Hide panels
#193#导航#Navigator
#194#尺寸#Scale
#195#角#Angle
#196# 下载新版本# Check what's new and download now
#197# 稍后提醒# Remind me later
#198#调色盘#Mixer
#199#适合旧版显卡#For older graphic cards
#200#放大光标#Zoom on cursor
#201#显示光标#Show brush cursor
#202# 重置颜色#Reset colors
#203#图像清楚画面清晰度#Canvas sharpness on zoom out
#204#全球比例#Global scale
#205#比例缩放速度#Zoom speed
#206#画笔比例缩放速度#Brush resize speed
#207#放大像素化#Pixelate on zoomin
#208#颜色样板#Swatches
#209# 倍增# Multiply
#210# 减去# Substract
#211# 高度# Height
#212#工具#Tools
#213#  文件#  File
#214#  编辑#  Edit
#215#  图像#  Image
#216#  图层#  Layer
#217#  选中#  Selection
#218#  视图#  View
#219#  其它#  Other
#220#关闭渗透#Close gaps
#221#间隙效应#Gaps effect
#222#添加#inc
#223#容许#Tolerance
#224#辅助图层#Ref. Layer
#225#全部图层#All layers
#226#从本图层取色#Pick from current layer
#227#取色管旁显示颜色#Show color bar near picker
#228#普通#Normal
#229#浑浊度#Opacity(pannel)
#230#忽略TAB#Ignore TAB
#231#悬停透明度#Normal opacity on select悬停透明度
#232#悬停尺寸#Normal scale on select
#233#快键:#Hotkeys:
#234#颜色：#Colors:
#235#模式：#Blend mode:
#236#考虑下层#Take underlayers color
#237#考虑下层#Take underlayers color
#238#纹理模式：#Texture mode:
#239# 倍增# Multiply
#240# 减去# Substract
#241# 高度# Height
#242#客户#Custom
#243#梯度#Quad gradient
#244#颜色梯度#Light gradient
#245#多梯度#Multi blend
#246#压力#Pressure
#247#圆形#Circle form
#248#衰减#Fades
#249#客户#Custom
#250#This gradient use MAIN and SECOND colors#This gradient use MAIN and SECOND colors
#251#This gradient use MAIN color and opacity#This gradient use MAIN color and opacity
#252#阴影#Shadows
#253#半影#Midtones
#254#明亮#Highlights
#255#清空比例#Reset scale
#256#清空颜色#Reset colors
#257#清空画笔#Load default brush
#258#保存画笔#Save as default brush
#259#单个模式：#Single forms:
#260#模型组:#Set of forms:
#261#OK (单个模式)#OK (Single form)
#262#OK (模型组)#OK (set of forms)
#263#新版本！#NEW VERSION!
#264#新版本可用 #New version is now available
#265#点#Rand points
#266#涂画#Rand stroke
#267#层序#Sequence
#268#随机比例#Rand scale
#269#取决压力#Pressure scale
#270#取决速度#Speed scale
#271#比例取决按力#Vertical scale on max pressure
#272#保存 #Save 
#273#选中!#Selection!
#274#清空底布倾斜度#Reset canvas angle
#275#画笔快速比例缩放#Brush resize by drag tool
#276#更改主次颜色#Swap main and second color
#277#取色管工具#Color picker tool
#278#画笔蘸色#Fill brush while drawing
#279#选中矩形#Select rect tool
#280#套索#Lasso tool
#281#注入#Fill tool
#282#梯度#Gradient tool
#283#按比例缩放#Zoom tool
#284#工具“手”#Hand tool
#285#新文件#New document
#286#打开文件#Open file
#287#色调及饱和度#Hue and saturation
#288#色平衡#Color balance
#289#复制图层#Duplicate layer
#290#新图层#New layer
#291#删除图层#Delete layer
#292#跟随向导#Snap to guides
#293#自区取色#Pick area color
#294#图层快速选择#Fast layer selection
#295#填充全部#Fill all
#296#删除选中内容#Delete from selection
#297#快键 1#Keyboard preset1
#298#快键 2#Keyboard preset2
#299#快键 3#Keyboard preset3
#300#快速关闭控制器#Fast disable brush controler
#301#另存为#Save As
#302#保存#Save
#303#选取补充色#Pick second color
#304#开/关向导#Enable-disable guides
#305#Swap tablet to fullscreen#Swap tablet to fullscreen
#306#旋转底布#Rotate canvas
#307#清除选中#Disable Selection
#308#魔术棒#Magic wand tool
#309#移动底布#Move canvas
#310#放大#Zoom In
#311#删除#Zoom out
#312#画笔尺寸+#Brush size +
#313#画笔尺寸-#Brush size -
#314#前一步#Undo
#315#快速比例缩放#Fast zoom tool
#316#后一步#Redo
#317#自由变换#Free transform
#318#移动图层#Move layer tool
#319#画笔#Brush tool
#320#取色管工具#Fast Color picker
#321#快速浏览#Fast preview
#322#粘合两图层#Collaps two layers
#323#复制#Copy
#324#粘贴#Paste
#325#反转选中#Invert selection
#326#剪切#Cut
#327#选中全部#Select all
#328#橡皮擦#Eraser tool
#329#工作区 1#Workspace 1
#330#工作区 2#Workspace 2
#331#工作区 3#Workspace 3
#332#工作区 4#Workspace 4
#333#工作区 5#Workspace 5
#334#工作区 6#Workspace 6
#335#隐藏选中#Hide selection
#336#反转底布#Reverse canvas
#337#全屏比例#Fit to screen
#338#100%比例#Fit to normal scale
#339#直线#Straight lines
#340#前一画笔#Previous brush
#341#线透明度#Guides opacity
#342#镜子数量#Number of mirrors
#343#复制#Duplication
#344#对称#Symmetry
#345#缩小复制品#Reduce duplicates size
#346#   :居中#   :acts to center
#347#对称#Toggle mirror
#348#橡皮擦模式#Toggle eraser
#349#开启图层透明度保护#Preserve layer opacity
#350#剪辑遮罩#Toggle clipping mask
#351#绳子#Rope
#352#弹簧#Spring
#353#构图#Crop
#354#选择语言#Choose Language
#355#绘图板设置#Tablet settings
#356#清空快键#Reset sliders keys
#357#锁定图层#Lock layer
#359#   设定 -#   Define –
#360#   设定 +#   Define +
#361#开启1、2、3...9#Enable 1,2,3..9 
#362#重置#Reset
#363#速度#Speed
#364#开启快键  #Define keys for
#365#显示/隐藏当前图层#Show/hide current layer
#366#根据X移动#X Offset
#367#根据Y移动#Y Offset
#368#宽度移动#Width Offset
#369#高度移动#Height Offset
#370#鼠标坐标模式#Coordinate mouse mode
#371#开启橡皮擦翻转#Enable pen-revert eraser
#372#快速取色#Fast pick color
#373#网格尺寸#Grid spacing
#374#-清除清单-#-Clear List-
#375#最后的文件#Recent Files
#376#缩面尺寸#Thumbnail size
#377#清空设置#Reset to default
#378#运行UI#Load custom UI
#379#保存UI#Save custom UI
#380#清空全部#Reset All
#381#清空控制板状态#Reset panels layout
#382#清空控制板颜色#Reset panels colors
#383#清空界面颜色#Reset interface color
#384#清空客户控制板#Reset custom pannels
#385#清空颜色样板#Reset swatches
#386#清空快键#Reset hotkeys
#387#创建新梯度#Create new gradient
#388#t删除当前梯度#Delete current gradien
#389#导出梯度#Import gradients
#390#倒入梯度#Export gradients
#391#清空梯度#Reset gradients
#392#开启“scroll bars”#Enable scroll bars
#393#创建新类别#Create New Category
#394#删除当前类别#Remove Current Category
#395#更改类别图标#Change Category Icon
#396#类别重命名#Rename Category
#397#类别向上移动#Category Move UP
#398#类别向下移动#Category Move DOWN
#399#类别向左移动#Move Category List To LEFT
#400#类别移至顶端#Move Category List On TOP
#401#导入画笔#Import Brushes
#402#导出画笔#Export Brushes
#403#导入 ABR#Import ABR
#404#复位当前类别#Reset Current Category
#405#重置所有刷#Reset All Brushes
#406#导入新的形式#Import new form
#407#创建一个文件夹#Create new folder
#408#删除当前的形式#Delete current form
#409#删除当前文件夹#Delete current folder
#410#导入从ABR#Import forms from ABR
#411#创建新预设#Create new preset
#412#导入新的纹理#Import new texture
#413#删除当前的预置#Delete current preset
#414#新图标置#Import new Icon
#415#删除当前纹理#Delete current texture
#416#删除当前图标#Delete current icon
#417#-创建新类别-#-Create new category-
#418#适用于当前画笔#Apply to current brush
#419#进入名称:#Enter name:
#420#捕获表#Capture Form
#421#规格化标准化#Normalize
#422#面具#Mask
#423#同步#Syncronization
#424#重命名#Rename
#425#他的影响径向渐变#Affect on radial gradient
#426#合并下层到一个新的#Merge lower layers into one new
#427#统治者#Ruler
#428#椭圆#Ellipse
#429#2点#2-point
#430#2点透视#2-point perspective
#431#3点#3-point
#432#3点透视#3-point perspective
#433#全球压硬度#Global pressure hardness
#434#敷面膜#Apply mask
#435#选择层#Select layer
#436#模糊#Blur
#437#与图层缩略图界限#Thumbnails with Layer bounds
#438#显示刷圈#Show brush circle
#439#脚本#Script
#440#进展#Progress
#441#播放速度#Play speed
#442#笔触#Stroke
#443#更改刷#Change Brush
#444#最后一个动作:#Last Action:
#445#下一步行动:#Next Action:
#446#更改选项 #Change option 
#447#重置画笔开始设置#Reset brush to start settings
#448#相对于画布大小#Relative to canvas size
#449#刷子大小#Brush size
#450#记住偶合#Remember randoms
#451#暂停上刷复位#Pause on brush reset
#452#创建启动新的画布#Create new canvas on start
#453#切换统治者#Toggle rulers
#454#克隆#Clone
#455#禁用矩形#Disable rectangle
#456#  过滤器#Filters
#457#模糊#Blur
#458#强度#Strength
#459#预习#Preview
#460#锐化#Sharpen
#461#解析度:#Resolution:
#462#像素#Pixels
#463#英寸#Inches
#464#毫米#Millimeters
#465#色彩范围#Color range
#466#硬度#Hardness
#467#当前层#Current layer
#468#翻转水平#Flip horizontal
#469#垂直翻转#Flip Vertical
#470#经#Warp
#471#100%#Full
#472#50%#Half
#473#25%#Quarter
#474#15%#Draft
#475#重置网格#Reset Grid
#476#在图层下显示#Show Under Layers
#477#显示上层#Show Upper Layers
#478#请选择活动区域#Please choose active area and press OK
#479#适合:#Fit To:
#480#锁边#Pin Edges
#481#选择多个图层时，无法绘制#You can not draw when more than one layer is selected
#482#显示/隐藏参考图层#Show/hide reference layer
#483#快速圖層移動#Fast layer move
#484#无缝模式#Seamless mode
#485#显示框架#Show frame
#486#温度#Temperature
#487#着色#Tint
#488#图形模式:#Graph mode
#489#经典#Classic
#490#直行#Straight
#491#保存亮度#Preserve Luminosity
#492#所选图层#Selected layers
#493#智能色彩校正#Smart Color Correction
#494#适用于:#Apply to:
#495#添加新的颜色掩码#Add new color mask
#496#启用色调蒙版#Enable Hue mask
#497#启用饱和屏蔽#Enable Saturation mask
#498#启用光度掩模#Enable Luminosity mask
#499#去掉#Remove
#500#设置颜色#Set color
#501#从预设中删除#Delete from presets
#502#高级选项#Advanced options
#503#输出饱和#Output Saturation
#504#饱和对比度#Output Saturation Contrast
#505#渠道#Channel
#506#进口#Import
#507#出口#Export
#508#关闭文档#Close current document
#509#克隆方式:#Clone mode:
#510#主要#Main
#511#接口#Interface
#512#性能#Performance
#513#光标#Cursors
#514#OpenGL#OpenGL
#515#GPU加速#GPU Acceleration
#516#GPU刷毛刷 (OpenGL 4.3)#GPU for bristle brushes (OpenGL 4.3)
#517#自动禁用小笔刷的GPUs#Auto disable GPU for small brushes
#518#始终将画笔轮廓显示为圆形#Always show brush outline as circle
#519#绘图:#On Drawing:
#520#Undo 计数#History states
#521#历史记忆#History memory
#522#刷子最大量#Maximum amount of brushes
#523#启用大画布（不推荐）#Enable large canvases (Not recommended)
#524#（需要重启才能生效）#(Need restart to take effect)
#525#启用实验性GPU快速模式#Enable experimental GPU fast mode
#526#一个接一个#One by one
#527#重置刷子颜色#Reset brush color
#528#转换刷子 请稍候...#Converting brushes. Please wait...
#529#使用渐变滑块#Use gradient sliders
#530#透明層#Clear layer
#531#禁用轉換#Disable Transformation
#532#最大鏡子#Max mirrors
#533#鏡像的最大重複次數#Max duplications for mirrors
#534#上面移動圖層#Move layer above
#535#在下面移動圖層#Move layer below