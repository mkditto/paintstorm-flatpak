# PaintStorm Studio in a Sandbox

So basically what I've done here is created a sandbox w/ flatpak and have (kind
of) gotten it to run inside the sandbox. There are some file issues because it
can't find `/usr/share/paintstorm` and I don't know how to create it in flatpak's
read-only filesystem. The solution would be if the devs could change it to
search in `/app/share/paintstorm`.

Update: I have managed to hex edit the file and change the file path myself. It
does work quite well right now and should be usable.

## Why Did I Do This?

I love paintstorm as a piece of software, but I was frustrated at how the
behavior was inconsistent across linux distros (the famous "well on my system..."
problem that linux has). Flatpak should be able to create a sandboxed environment
that is identical across all systems meaning that problems encountered by one
user should be encountered by all users. Behavior be more consistent and would
save the devs time squashing bugs that only appear on some obscure linux distro.

With this flatpak I have included some additional libraries necessary to run
which can be found in `files/lib` which will automatically mount into `/app/lib`
when the you enter the flatpak sandbox. I also should have included all the
necessary headers if the devs want to try and build paintstorm inside this
sandbox.

## Running

Since this is flatpak, running should actually be pretty easy.

### Install flatpak

- #### Alpine
Since alpine linux uses a musl instead of glibc, I don't actually know if this
will work on alpine.
```
$ sudo apk add flatpak
```

- #### Arch
```
$ sudo pacman -S flatpak
```

- #### Debian
```
$ sudo apt install flatpak
```

- #### Fedora
```
$ sudo dnf install flatpak
```

- #### Gentoo
[Good luck buddy](https://github.com/fosero/flatpak-overlay)

- #### openSUSE
```
$ sudo zypper install flatpak
```

- #### Ubuntu
Now a version of flatpak is available in Ubuntu 16.10 and newer via Universe,
but this is an older version and it is recommended that you use the on in the
following PPA.
```
$ sudo add-apt-repository ppa:alexlarsson/flatpak
$ sudo apt update
$ sudo apt install flatpak
```

### Install Runtimes and SDKs
For this I am using the gnome runtime and sdk version 3.22 so you'll need to
install that.
```
$ flatpak remote-add --from gnome https://sdk.gnome.org/gnome.flatpakrepo
$ flatpak install gnome org.gnome.Platform//3.22
$ flatpak install gnome org.gnome.Sdk//3.22

```

### Now For the Fun Part
Just `cd` in the git repo after you've cloned it and run:
```
./start.sh
```

Congratulations! You should now be inside the flatpak sandbox shell! From here
you should be able to run (or attempt to run) paintstorm with
```
./files/bin/paintstorm
```
